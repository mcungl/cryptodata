package com.emsi.cryptodata.activity.presenter;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.emsi.cryptodata.activity.views.MainActivityViewImpl;

public class MainActivity extends AppCompatActivity {

	private MainActivityViewImpl mMainActivityViewImpl;
	private ViewPager mViewPager;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mMainActivityViewImpl = new MainActivityViewImpl(this, null);
		setContentView(mMainActivityViewImpl.getRootView());
	}

}
