package com.emsi.cryptodata.activity.viewpageradapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.emsi.cryptodata.fragment.presenter.DataFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {
	private static final String[] CURRENCIES = {"BTC", "ETH", "XRP", "LTC"};

	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		DataFragment fragment = new DataFragment();
		Bundle bundle = new Bundle();
		bundle.putString("currency", CURRENCIES[position]);
		fragment.setArguments(bundle);

		return fragment;
	}

	@Override
	public int getCount() {
		return CURRENCIES.length;
	}
}
