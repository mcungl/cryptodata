package com.emsi.cryptodata.activity.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emsi.cryptodata.R;
import com.emsi.cryptodata.activity.viewpageradapter.ViewPagerAdapter;

public class MainActivityViewImpl implements MainActivityViewInterface {

	private Context mContext;
	private View mRootView;
	private ViewPager mViewPager;

	public MainActivityViewImpl(Context context, ViewGroup container) {
		mContext = context;

		mRootView = LayoutInflater.from(context).inflate(R.layout.activity_main, container);
		mViewPager = (ViewPager) mRootView.findViewById(R.id.viewpager);

		AppCompatActivity appCompatActivity = (AppCompatActivity) context;
		ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(appCompatActivity.getSupportFragmentManager());
		mViewPager.setAdapter(pagerAdapter);

	}

	@Override
	public View getRootView() {
		return mRootView;
	}

	@Override
	public ViewPager getViewPager() {
		return mViewPager;
	}
}
