package com.emsi.cryptodata.activity.views;

import android.support.v4.view.ViewPager;

import com.emsi.cryptodata.view.ViewMvp;

public interface MainActivityViewInterface extends ViewMvp {
	ViewPager getViewPager();
}
