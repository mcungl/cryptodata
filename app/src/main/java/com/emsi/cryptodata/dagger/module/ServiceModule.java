package com.emsi.cryptodata.dagger.module;

import com.emsi.cryptodata.data.Service;
import com.emsi.cryptodata.data.memory.MemoryCache;
import com.emsi.cryptodata.data.network.RetrofitInterface;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class ServiceModule {

	String mBaseUrl;

	public ServiceModule(String mBaseUrl) {
		this.mBaseUrl = mBaseUrl;
	}

	@Provides
	@Singleton
	Retrofit providesRetrofit() {

		HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
		interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

		OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(interceptor).build();

		return new Retrofit.Builder()
				.baseUrl(mBaseUrl)
				.client(okHttpClient)
				.addConverterFactory(GsonConverterFactory.create())
				.addConverterFactory(ScalarsConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.build();
	}


	@Provides
	@Singleton
	public MemoryCache providesMemoryCache() {
		return new MemoryCache();
	}

	@Provides
	@Singleton
	@SuppressWarnings("unused")
	public RetrofitInterface providesRetrofitInterface(
			Retrofit retrofit) {
		return retrofit.create(RetrofitInterface.class);
	}

	@Provides
	@Singleton
	@SuppressWarnings("unused")
	public Service providesService(
			RetrofitInterface retrofitInterface, MemoryCache memoryCache) {
		return new Service(retrofitInterface, memoryCache);
	}

}
