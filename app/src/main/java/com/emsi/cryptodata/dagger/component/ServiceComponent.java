package com.emsi.cryptodata.dagger.component;

import com.emsi.cryptodata.dagger.module.ServiceModule;
import com.emsi.cryptodata.fragment.presenter.DataFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ServiceModule.class,})
public interface ServiceComponent {
	void inject(DataFragment activity);
}
