package com.emsi.cryptodata;

import android.app.Application;

import com.androidplot.util.PixelUtils;
import com.emsi.cryptodata.dagger.component.DaggerServiceComponent;
import com.emsi.cryptodata.dagger.component.ServiceComponent;
import com.emsi.cryptodata.dagger.module.ServiceModule;


public class AndroidPlotTestApplication extends Application {

	private ServiceComponent mServiceComponent;

	@Override
	public void onCreate() {
		super.onCreate();

		PixelUtils.init(this);
		mServiceComponent = DaggerServiceComponent.builder().serviceModule(new ServiceModule("https://min-api.cryptocompare.com")).build();
	}

	public ServiceComponent getServiceComponent() {
		return mServiceComponent;
	}
}
