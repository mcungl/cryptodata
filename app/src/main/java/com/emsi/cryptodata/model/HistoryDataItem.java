package com.emsi.cryptodata.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class HistoryDataItem {

	private Date time;
	@SerializedName("close")
	private double value;
	@SerializedName("volumefrom")
	private double volume;

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}
}
