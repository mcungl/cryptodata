package com.emsi.cryptodata.model;

import com.androidplot.xy.EditableXYSeries;

import java.util.List;

public class HistorySeries implements EditableXYSeries {
	protected List<HistoryDataItem> items;
	protected String title;

	public HistorySeries(String title, List<HistoryDataItem> items) {
		this.title = title;
		this.items = items;
	}

	@Override
	public void setX(Number x, int index) {

	}

	@Override
	public void setY(Number y, int index) {

	}

	@Override
	public void resize(int size) {

	}

	@Override
	public int size() {
		return items.size();
	}

	@Override
	public Number getX(int index) {
		return index;
	}

	@Override
	public Number getY(int index) {
		return items.get(index).getValue();
	}

	@Override
	public String getTitle() {
		return title;
	}

	public HistoryDataItem getDataItem(int index) {
		if (items != null && index < items.size()) {
			return items.get(index);
		}
		return null;
	}

	public List<HistoryDataItem> getItems() {
		return items;
	}

}
