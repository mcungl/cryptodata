package com.emsi.cryptodata.model;

import java.util.List;

public class HistoryVolumeSeries extends HistorySeries {

	public HistoryVolumeSeries(String title, List<HistoryDataItem> items) {
		super(title, items);
	}

	@Override
	public Number getY(int index) {
		return items.get(index).getVolume();
	}
}
