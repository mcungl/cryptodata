package com.emsi.cryptodata.fragment.views;

import com.emsi.cryptodata.model.HistorySeries;
import com.emsi.cryptodata.model.HistoryVolumeSeries;
import com.emsi.cryptodata.view.ViewMvp;

public interface DataFragmentViewInterface extends ViewMvp {

	void bindTitle(String title);

	void bindPriceChart(HistorySeries data);

	void bindVolumeChart(HistoryVolumeSeries data);

	void showError(String message);

}
