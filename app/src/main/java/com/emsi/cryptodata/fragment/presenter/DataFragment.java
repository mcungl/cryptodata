package com.emsi.cryptodata.fragment.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.emsi.cryptodata.AndroidPlotTestApplication;
import com.emsi.cryptodata.data.Service;
import com.emsi.cryptodata.fragment.views.DataFragmentViewImpl;
import com.emsi.cryptodata.model.HistoryDataItem;
import com.emsi.cryptodata.model.HistorySeries;
import com.emsi.cryptodata.model.HistoryVolumeSeries;

import java.util.List;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;

public class DataFragment extends Fragment {

	private DataFragmentViewImpl mDatafragmentViewImpl;
	private CompositeSubscription mSubscription;
	private String mCurrency;

	@Inject
	Service mService;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		((AndroidPlotTestApplication) getActivity().getApplication()).getServiceComponent().inject(this);
		super.onCreate(savedInstanceState);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		mDatafragmentViewImpl = new DataFragmentViewImpl(getActivity(), container, inflater);
		return mDatafragmentViewImpl.getRootView();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Bundle bundle = getArguments();
		mCurrency = bundle.getString("currency");

		mSubscription = new CompositeSubscription();

		mDatafragmentViewImpl.bindTitle(mCurrency);

		mSubscription.add(mService.getLast30Days(mCurrency, new Service.GetLast30DaysCallback() {
			@Override
			public void onSuccess(List<HistoryDataItem> response) {
				mDatafragmentViewImpl.bindPriceChart(new HistorySeries("Price in USD", response));
				mDatafragmentViewImpl.bindVolumeChart(new HistoryVolumeSeries("Volume", response));
			}

			@Override
			public void onError(Throwable error) {
				mDatafragmentViewImpl.showError(error.getLocalizedMessage());
			}
		}));

		mSubscription.add(mService.getPriceInUsd(mCurrency, new Service.GetPriceInUsdCallback() {
			@Override
			public void onSuccess(Double response) {
				mDatafragmentViewImpl.bindTitle(mCurrency + " $" + String.valueOf(response));
			}

			@Override
			public void onError(Throwable error) {
				mDatafragmentViewImpl.showError(error.getLocalizedMessage());
			}
		}));
	}

	@Override
	public void onStop() {
		super.onStop();
		mSubscription.unsubscribe();
	}
}
