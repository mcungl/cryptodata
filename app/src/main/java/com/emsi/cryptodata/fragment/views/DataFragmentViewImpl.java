package com.emsi.cryptodata.fragment.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.emsi.cryptodata.R;
import com.emsi.cryptodata.model.HistorySeries;
import com.emsi.cryptodata.model.HistoryVolumeSeries;
import com.emsi.cryptodata.view.BarChartView;
import com.emsi.cryptodata.view.LineChartView;

public class DataFragmentViewImpl implements DataFragmentViewInterface {

	private Context mContext;
	private View mRootView;

	private TextView mTitleView;
	private LineChartView mLineChart;
	private BarChartView mBarChart;

	public DataFragmentViewImpl(Context context, ViewGroup container, LayoutInflater inflater) {
		mContext = context;
		mRootView = inflater.inflate(R.layout.fragment_data, container, false);

		mTitleView = (TextView) mRootView.findViewById(R.id.title);
		mLineChart = (LineChartView) mRootView.findViewById(R.id.linechart);
		mBarChart = (BarChartView) mRootView.findViewById(R.id.barchart);


	}

	@Override
	public View getRootView() {
		return mRootView;
	}


	@Override
	public void bindTitle(String title) {
		mTitleView.setText(title);
	}

	@Override
	public void bindPriceChart(HistorySeries data) {
		mLineChart.addSeries(data);
	}

	@Override
	public void bindVolumeChart(HistoryVolumeSeries data) {
		mBarChart.addSeries(data);
	}

	@Override
	public void showError(String message) {
		Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
	}
}
