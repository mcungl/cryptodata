package com.emsi.cryptodata.view;

import android.content.Context;
import android.util.AttributeSet;

import com.androidplot.xy.LineAndPointFormatter;
import com.emsi.cryptodata.R;
import com.emsi.cryptodata.model.HistorySeries;

public class LineChartView extends Chart {

	private LineAndPointFormatter mFormatter;

	public LineChartView(Context context, String title) {
		super(context, title);
		init();
	}

	public LineChartView(Context context, String title, RenderMode mode) {
		super(context, title, mode);
		init();
	}

	public LineChartView(Context context, AttributeSet attributes) {
		super(context, attributes);
		init();
	}

	public LineChartView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	protected void init() {
		super.init();
		mFormatter = new LineAndPointFormatter(getContext(), R.xml.line_point_foramtter);
	}

	public void addSeries(HistorySeries series) {
		mSeries = series;
		addSeries(mSeries, mFormatter);
		invalidate();
	}
}
