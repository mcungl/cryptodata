package com.emsi.cryptodata.view;

import android.content.Context;
import android.util.AttributeSet;

import com.androidplot.xy.BarFormatter;
import com.emsi.cryptodata.model.HistoryVolumeSeries;

import java.text.SimpleDateFormat;

public class BarChartView extends Chart {

	private BarFormatter mFormatter;

	private final SimpleDateFormat dt = new SimpleDateFormat("dd. MMM");

	public BarChartView(Context context, String title) {
		super(context, title);
		init();
	}

	public BarChartView(Context context, String title, RenderMode mode) {
		super(context, title, mode);
		init();
	}

	public BarChartView(Context context, AttributeSet attributes) {
		super(context, attributes);
		init();
	}

	public BarChartView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	protected void init() {
		super.init();
		mFormatter = new BarFormatter();
	}


	public void addSeries(HistoryVolumeSeries series) {
		mSeries = series;
		addSeries(mSeries, mFormatter);
		invalidate();
	}

}
