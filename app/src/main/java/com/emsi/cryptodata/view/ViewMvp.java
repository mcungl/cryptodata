package com.emsi.cryptodata.view;

import android.view.View;

public interface ViewMvp {
	View getRootView();
}
