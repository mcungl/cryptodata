package com.emsi.cryptodata.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.androidplot.ui.Insets;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYSeriesFormatter;
import com.emsi.cryptodata.model.HistoryDataItem;
import com.emsi.cryptodata.model.HistorySeries;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class Chart extends XYPlot {

	protected final SimpleDateFormat dt = new SimpleDateFormat("HH:mm");
	protected HistorySeries mSeries;

	public Chart(Context context, String title) {
		super(context, title);
		init();
	}

	public Chart(Context context, String title, RenderMode mode) {
		super(context, title, mode);
		init();
	}

	public Chart(Context context, AttributeSet attributes) {
		super(context, attributes);
		init();
	}

	public Chart(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	protected void init() {
		getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(mBottomLineLabelsFormat);
		getGraph().getLineLabelStyle(XYGraphWidget.Edge.RIGHT).setFormat(mRightLineLabelsFormat);

		float dp16 = PixelUtils.dpToPix(16);
		float dp30 = PixelUtils.dpToPix(30);
		float dp11 = PixelUtils.dpToPix(11);

		getGraph().setMargins(dp16, dp16, dp16, dp16);
		getGraph().getLineLabelInsets().setRight(dp11);
		getLegend().setVisible(false);
		getGraph().setLineLabelEdges(
				XYGraphWidget.Edge.RIGHT,
				XYGraphWidget.Edge.BOTTOM);

		getGraph().getLineLabelStyle(XYGraphWidget.Edge.RIGHT).getPaint().setTextSize(dp11);
		getGraph().getLineLabelStyle(XYGraphWidget.Edge.RIGHT).getPaint().setColor(Color.DKGRAY);
		getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).getPaint().setTextSize(dp11);
		getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).getPaint().setColor(Color.DKGRAY);

		setBackgroundPaint(null);
		getGraph().setBackgroundPaint(null);
		getGraph().setGridBackgroundPaint(null);

		getGraph().setGridInsets(new Insets(0, dp16, 0, dp30));

		setRangeStep(StepMode.SUBDIVIDE, 4);
		setDomainStep(StepMode.SUBDIVIDE, 7);

		setBorderPaint(null);
	}

	@Override
	public synchronized boolean addSeries(XYSeries series, XYSeriesFormatter formatter) {
		setTitle(series.getTitle());
		return super.addSeries(series, formatter);
	}

	protected final Format mBottomLineLabelsFormat = new Format() {
		@Override
		public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
			int i = Math.round(((Number) obj).floatValue());

			if (mSeries != null && i < mSeries.size()) {
				HistoryDataItem item = mSeries.getDataItem(i);
				if (item != null) {
					return toAppendTo.append(dt.format(item.getTime()));
				}
			}
			return toAppendTo.append("");
		}

		@Override
		public Object parseObject(String source, ParsePosition pos) {
			return null;
		}
	};

	protected final Format mRightLineLabelsFormat = new Format() {
		@Override
		public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
			double i = ((Number) obj).doubleValue();
			return toAppendTo.append(Chart.format(i));
		}

		@Override
		public Object parseObject(String source, ParsePosition pos) {
			return null;
		}
	};


	private static final NavigableMap<Long, String> suffixes = new TreeMap<>();

	static {
		suffixes.put(1_000L, "k");
		suffixes.put(1_000_000L, "M");
		suffixes.put(1_000_000_000L, "G");
		suffixes.put(1_000_000_000_000L, "T");
		suffixes.put(1_000_000_000_000_000L, "P");
		suffixes.put(1_000_000_000_000_000_000L, "E");
	}

	public static String format(double value) {
		//Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
		if (value == Long.MIN_VALUE) return format(Long.MIN_VALUE + 1);
		if (value < 0) return "-" + format(-value);
		if (value < 10) return String.format("%.2f", value);
		if (value < 10000) return Long.toString((long) value); //deal with easy case

		Map.Entry<Long, String> e = suffixes.floorEntry((long) value);
		Long divideBy = e.getKey();
		String suffix = e.getValue();

		long truncated = (long) value / (divideBy / 10); //the number part of the output times 10
		boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
		return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
	}

}
