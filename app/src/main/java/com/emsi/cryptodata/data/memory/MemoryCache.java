package com.emsi.cryptodata.data.memory;


import android.util.Log;

import com.emsi.cryptodata.model.HistoryDataItem;

import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

public class MemoryCache {

	private HashMap<String, List<HistoryDataItem>> last30Days = new HashMap<>();
	private HashMap<String, Double> priceInUsd = new HashMap<>();

	public Observable<List<HistoryDataItem>> getLast30Days(final String fsym) {
		Observable<List<HistoryDataItem>> observable = Observable.create(new Observable.OnSubscribe<List<HistoryDataItem>>() {
			@Override
			public void call(Subscriber<? super List<HistoryDataItem>> subscriber) {
				subscriber.onNext(last30Days.get(fsym));
				subscriber.onCompleted();
			}
		});

		return observable;
	}

	public void saveLast30Days(final String fsym, final List<HistoryDataItem> data) {
		last30Days.put(fsym, data);
	}

	public Observable<Double> getPriceInUsd(final String fsym) {
		Observable<Double> observable = Observable.create(new Observable.OnSubscribe<Double>() {
			@Override
			public void call(Subscriber<? super Double> subscriber) {
				subscriber.onNext(priceInUsd.get(fsym));
				subscriber.onCompleted();
			}
		});

		return observable;
	}

	public void savePriceInUsd(final String fsym, final Double data) {
		priceInUsd.put(fsym, data);
	}
}
