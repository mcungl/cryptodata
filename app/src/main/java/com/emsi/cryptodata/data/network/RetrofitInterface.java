package com.emsi.cryptodata.data.network;


import com.google.gson.JsonElement;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface RetrofitInterface {

	@GET("/data/histominute?tsym=USD&limit=30&aggregate=30&e=Kraken")
	Observable<JsonElement> getLast30Days(@Query("fsym") String fsym);

	@GET("/data/price?tsyms=USD&e=Kraken")
	Observable<JsonElement> getPriceInUsd(@Query("fsym") String fsym);

}
