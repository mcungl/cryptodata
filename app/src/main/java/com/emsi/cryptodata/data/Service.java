package com.emsi.cryptodata.data;

import com.emsi.cryptodata.data.memory.MemoryCache;
import com.emsi.cryptodata.data.network.RetrofitInterface;
import com.emsi.cryptodata.model.HistoryDataItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class Service {

	private final RetrofitInterface mRetrofitInterface;
	private final MemoryCache mMemoryCache;

	public Service(RetrofitInterface retrofitInterface, MemoryCache memoryCache) {
		mRetrofitInterface = retrofitInterface;
		mMemoryCache = memoryCache;
	}

	public Subscription getLast30Days(final String currency, final GetLast30DaysCallback callback) {
		return Observable.concat(
				mMemoryCache.getLast30Days(currency),
				mRetrofitInterface.getLast30Days(currency)
						.flatMap(new Func1<JsonElement, Observable<List<HistoryDataItem>>>() {
							@Override
							public Observable<List<HistoryDataItem>> call(JsonElement jsonElement) {
								Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new MyDeserializer()).create();
								HistoryDataItem[] historyDataItems = gson.fromJson(jsonElement.getAsJsonObject().get("Data"), HistoryDataItem[].class);
								return Observable.just(Arrays.asList(historyDataItems));
							}
						}).doOnNext(new Action1<List<HistoryDataItem>>() {
					@Override
					public void call(List<HistoryDataItem> data) {
						mMemoryCache.saveLast30Days(currency, data);
					}
				})).first(new Func1<List<HistoryDataItem>, Boolean>() {
			@Override
			public Boolean call(List<HistoryDataItem> s) {
				return (s != null);
			}
		})
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.onErrorResumeNext(new Func1<Throwable, Observable<? extends List<HistoryDataItem>>>() {
					@Override
					public Observable<? extends List<HistoryDataItem>> call(Throwable throwable) {
						return Observable.error(throwable);
					}
				}).subscribe(new Subscriber<List<HistoryDataItem>>() {
					@Override
					public void onCompleted() {

					}

					@Override
					public void onError(Throwable e) {
						callback.onError(e);
					}

					@Override
					public void onNext(List<HistoryDataItem> s) {
						callback.onSuccess(s);
					}
				});
	}

	public class MyDeserializer implements JsonDeserializer<Date> {

		@Override
		public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return new Date(json.getAsLong() * 1000);
		}
	}

	public Subscription getPriceInUsd(final String currency, final GetPriceInUsdCallback callback) {
		return Observable.concat(
				mMemoryCache.getPriceInUsd(currency),
				mRetrofitInterface.getPriceInUsd(currency)
						.flatMap(new Func1<JsonElement, Observable<Double>>() {
							@Override
							public Observable<Double> call(JsonElement jsonElement) {
								try {
									return Observable.just(jsonElement.getAsJsonObject().get("USD").getAsDouble());
								} catch (Exception e) {
									e.printStackTrace();
									return Observable.just(Double.valueOf(0));
								}
							}
						}).doOnNext(new Action1<Double>() {
					@Override
					public void call(Double data) {
						mMemoryCache.savePriceInUsd(currency, data);
					}
				})).first(new Func1<Double, Boolean>() {
			@Override
			public Boolean call(Double aDouble) {
				return aDouble != null;
			}
		})
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.onErrorResumeNext(new Func1<Throwable, Observable<? extends Double>>() {
					@Override
					public Observable<? extends Double> call(Throwable throwable) {
						return Observable.error(throwable);
					}
				}).first(new Func1<Double, Boolean>() {
					@Override
					public Boolean call(Double d) {
						return (d != null);
					}
				}).subscribe(new Subscriber<Double>() {
					@Override
					public void onCompleted() {
					}

					@Override
					public void onError(Throwable e) {
						callback.onError(e);
					}

					@Override
					public void onNext(Double d) {
						callback.onSuccess(d);
					}
				});
	}

	public interface GetLast30DaysCallback {
		void onSuccess(List<HistoryDataItem> response);

		void onError(Throwable error);
	}

	public interface GetPriceInUsdCallback {
		void onSuccess(Double response);

		void onError(Throwable error);
	}
}